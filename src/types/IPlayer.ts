export interface IPlayer {
  id: number;
  name: string;
  ping: number;
  hwId: string;
  scId: number;
  scName: string;
  ip: string;
  connectedSince: number;
}
