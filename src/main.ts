import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import i18n from "./i18n";
import "./assets/tailwind.css";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

createApp(App)
  .use(i18n)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(router)
  .mount("#app");
