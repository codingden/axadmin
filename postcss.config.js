module.exports = {
  plugins: {
    tailwindcss: {
      variants: {
        extend: {
          borderRadius: ["hover"],
        },
      },
    },
    autoprefixer: {},
  },
};
